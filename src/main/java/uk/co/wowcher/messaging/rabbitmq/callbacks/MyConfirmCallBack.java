package uk.co.wowcher.messaging.rabbitmq.callbacks;

import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;

public class MyConfirmCallBack implements RabbitTemplate.ConfirmCallback
{
    private static final Logger logger = Logger.getLogger(MyConfirmCallBack.class.getName());

    @Override
	public void confirm(CorrelationData correlationData, boolean b)
	{
        if (logger.isDebugEnabled())
        {
            logger.debug("confirm()" + correlationData.getId());
        }
	}
}
