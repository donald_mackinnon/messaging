package uk.co.wowcher.messaging.rabbitmq;

// TODO: Auto-generated Javadoc

import org.apache.log4j.Logger;

/**
 * The Class PojoBean.
 */
public class PojoBean
{
    private static final Logger logger = Logger.getLogger(PojoBean.class.getName());

	/** The msg. */
	private String msg;

	/**
	 * Instantiates a new pojo bean.
	 */
	public PojoBean()
	{

	}

	/**
	 * Gets the msg.
	 * 
	 * @return the msg
	 */
	public String getMsg()
	{
		return msg;
	}

	public void receive(String message)
	{
        if (logger.isDebugEnabled())
        {
            logger.debug("receive() " + message);
        }
	}


}
