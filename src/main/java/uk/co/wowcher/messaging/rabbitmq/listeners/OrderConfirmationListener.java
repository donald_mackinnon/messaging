package uk.co.wowcher.messaging.rabbitmq.listeners;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;

import uk.co.wowcher.messaging.rabbitmq.PojoBean;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving orderConfirmation events.
 * The class that is interested in processing a orderConfirmation
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addOrderConfirmationListener<code> method. When
 * the orderConfirmation event occurs, that object's appropriate
 * method is invoked.
 *
 * @see OrderConfirmationEvent
 */
public class OrderConfirmationListener implements MessageListener
{
    private static final Logger logger = Logger.getLogger(OrderConfirmationListener.class.getName());

	/** The pojo bean. */
	private PojoBean pojoBean;
	private RabbitTemplate  amqpTemplate ;
	private MessageConverter messageConverter;

	/**
	 * Instantiates a new order confirmation listener.
	 */
	public OrderConfirmationListener()
	{
        if (logger.isDebugEnabled())
        {
            logger.debug("OrderConfirmationListener()");
        }
		pojoBean = new PojoBean();

		amqpTemplate = new RabbitTemplate();
		
		// Save message converted
		messageConverter = amqpTemplate.getMessageConverter();


	}

	/* (non-Javadoc)
	 * @see org.springframework.amqp.core.MessageListener#onMessage(org.springframework.amqp.core.Message)
	 */
	public void onMessage(Message message)
	{
		Object obj = messageConverter.fromMessage(message);
        if (obj.getClass().isInstance("java.lang.String"))
        {
            String msg = new String(message.getBody());
            if (logger.isDebugEnabled())
            {
                logger.debug("\t OrderConfirmationListener consumer = \" + msg");
            }
            pojoBean.receive(msg);
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("\t OrderConfirmationListener consumer = " + obj.toString());
            }

        }
		//JsonMessageConverter jmc = new JsonMessageConverter();
		//User u = (User) jmc.fromMessage(message);

	}


}
