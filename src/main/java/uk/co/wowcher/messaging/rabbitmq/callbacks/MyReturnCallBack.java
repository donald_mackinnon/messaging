package uk.co.wowcher.messaging.rabbitmq.callbacks;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public class MyReturnCallBack implements RabbitTemplate.ReturnCallback
{
    private static final Logger logger = Logger.getLogger(MyReturnCallBack.class.getName());

    @Override
	public void returnedMessage(Message message, int i, String s, String s1, String s2)
	{
		String msg = new String(message.getBody());

        if (logger.isDebugEnabled())
        {
            logger.debug("returnedMessage()");
            logger.debug("\t -----------------------------------------------");
            logger.debug("\t Returned Message: \" + msg + \" s = \" + s + \" s1 = \" + s1 + \" s2 = \" + s2 ");
            logger.debug("\t -----------------------------------------------");
        }
	}
}
