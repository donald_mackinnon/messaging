package uk.co.wowcher.messaging.rabbitmq;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;

import uk.co.wowcher.messaging.rabbitmq.callbacks.MyConfirmCallBack;
import uk.co.wowcher.messaging.rabbitmq.callbacks.MyReturnCallBack;

// TODO: Auto-generated Javadoc

/**
 * The Class RabbitQ.
 */
@Resource
public class RabbitProducer {
    private static final Logger logger = Logger.getLogger(RabbitProducer.class.getName());

    /**
     * The rabbit q.
     */
    // @Autowired
    // @Qualifier("emailTemplate")
    private RabbitTemplate template;


    /**
     * Instantiates a new rabbit q.
     */
    public RabbitProducer() {
    }

    /**
     * Sets the template.
     *
     * @param template the new template
     */
    public void setTemplate(RabbitTemplate template) {
        if (logger.isDebugEnabled()) {
            logger.debug("setTemplate()");
            logger.debug("\t Host           = " + template.getConnectionFactory().getHost());
            logger.debug("\t Port           = " + template.getConnectionFactory().getPort());
            logger.debug("\t VirtualHost    = " + template.getConnectionFactory().getVirtualHost());
        }

        this.template = template;

        MyReturnCallBack mReturnCallBack = new MyReturnCallBack();
        template.setReturnCallback(mReturnCallBack);

        MyConfirmCallBack myConfirmCallBack = new MyConfirmCallBack();
        template.setConfirmCallback(myConfirmCallBack);

    }

    /**
     * Send message.
     *
     * @param msg the msg
     */
    public void sendMessage(String msg, String id) {
        if (logger.isDebugEnabled()) {
            logger.debug("sendMessage() msg = " + msg + " id = " + id);
        }

        //template.convertAndSend(msg);
        CorrelationData correlationData = new CorrelationData(id);
        template.correlationconvertAndSend(msg, correlationData);
        // template.convertAndSend("dosomething", "key","foriffo", new CorrelationData("123"));

    }


    public void sendMessage(Object obj, String id) {
        if (logger.isDebugEnabled()) {
            logger.debug("sendMessage() obj = " + obj.toString() + " id = " + id);
        }

        //template.convertAndSend(msg);
        CorrelationData correlationData = new CorrelationData(id);
        template.correlationconvertAndSend(obj, correlationData);
        // template.convertAndSend("dosomething", "key","foriffo", new CorrelationData("123"));

    }


    /**
     * Gets the template.
     *
     * @return the template
     */
    public RabbitTemplate getTemplate() {
        return template;
    }

}
