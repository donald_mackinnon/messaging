package uk.co.wowcher.messaging.rabbitmq.listeners;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;

import uk.co.wowcher.messaging.rabbitmq.PojoBean;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving email events.
 * The class that is interested in processing a email
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addEmailListener<code> method. When
 * the email event occurs, that object's appropriate
 * method is invoked.
 *
 * @see EmailEvent
 */
public class RefundRequestedEmailForCustomerListener implements MessageListener
{
    private static final Logger logger = Logger.getLogger(RefundRequestedEmailForCustomerListener.class.getName());

	/** The pojo bean. */
	private PojoBean pojoBean;
	private RabbitTemplate  amqpTemplate ;
	private MessageConverter messageConverter;

	/**
	 * Instantiates a new email listener.
	 */
	public RefundRequestedEmailForCustomerListener()
	{
        if (logger.isDebugEnabled())
        {
            logger.debug("RefundRequestedEmailForCustomerListener()");
        }
		pojoBean = new PojoBean();

		amqpTemplate = new RabbitTemplate();
		
		// Save message converted
		messageConverter = amqpTemplate.getMessageConverter();


	}

	/* (non-Javadoc)
	 * @see org.springframework.amqp.core.MessageListener#onMessage(org.springframework.amqp.core.Message)
	 */
	public void onMessage(Message message)
	{
		Object obj = messageConverter.fromMessage(message);
        if (obj.getClass().isInstance("java.lang.String"))
        {
            String msg = new String(message.getBody());
            if (logger.isDebugEnabled())
            {
                logger.debug("\t RefundRequestedEmailForCustomerListener consumer = \" + msg");
            }
            pojoBean.receive(msg);
        }
        else
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("\t RefundRequestedEmailForCustomerListener consumer = " + obj.toString());
            }

        }


		//JsonMessageConverter jmc = new JsonMessageConverter();
		//User u = (User) jmc.fromMessage(message);

	}


}
