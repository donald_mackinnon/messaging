package uk.co.wowcher.messaging.rabbitmq.producer;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.xml" })
// @ContextConfiguration(locations = { "classpath:/rabbitmq-producer-context.xml",
// "classpath:/rabbitmq-listener-context.xml" })
@ContextConfiguration(locations = {"classpath:/spring/rabbitmq-producer-context.xml", "classpath:/spring/giftPrintableWowcherListener.xml"})
public class GiftPrintableWowcherTest
{
    private static final Logger logger = Logger.getLogger(GiftPrintableWowcherTest.class.getName());

	/** The gift printable wowcher producer. */
	@Autowired
	@Qualifier("giftPrintableWowcherProducer")
	private RabbitProducer giftPrintableWowcherProducer;


	/**
	 * Sets the up before class.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
        BasicConfigurator.configure();
	}

    /**
     * Gift printable wowcher test.
     */
    @Test
    public void giftPrintableWowcherTest()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("giftPrintableWowcherTest()");
        }

        try
        {
            giftPrintableWowcherProducer.sendMessage("giftPrintableWowcherTest!", "1111");

            Thread.sleep(1000);

        }
        catch (Exception e)
        {
            fail("Test failed: " + e.getLocalizedMessage());
        }
    }



}
