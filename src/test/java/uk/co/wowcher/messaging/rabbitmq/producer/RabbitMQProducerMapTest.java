package uk.co.wowcher.messaging.rabbitmq.producer;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.xml" })
@ContextConfiguration(locations = {"classpath:/spring/rabbitmq-producer-context.xml", "classpath:/spring/rpxListener.xml",
    "classpath:/spring/refundRequestedEmailForCustomerListener.xml",
    "classpath:/spring/refundRequestedEmailForMerchantListener.xml",
    "classpath:/spring/refundDeclinedEmailForCustomerListener.xml", "classpath:/spring/printableWowcherListener.xml",
    "classpath:/spring/paymentReceiptListener.xml", "classpath:/spring/orderEntryListener.xml",
    "classpath:/spring/netStorageFtpListener.xml", "classpath:/spring/giftPrintableWowcherListener.xml",
    "classpath:/spring/frontEndPasswordResetEmailListener.xml", "classpath:/spring/emailListener.xml",
    "classpath:/spring/deliveryAddressDealGiftEmailListener.xml", "classpath:/spring/dealVoucherRefundListener.xml",
    "classpath:/spring/customerSubscriptionSecondaryListener.xml", "classpath:/spring/customerSubscriptionPrimaryListener.xml"})
public class RabbitMQProducerMapTest
{
    private static final Logger logger = Logger.getLogger(RabbitMQProducerMapTest.class.getName());

	/** The rabbit producer. */
	@Autowired
	@Qualifier("emailProducer")
	private RabbitProducer emailProducer;

	/** The rabbit producer. */
	@Autowired
	@Qualifier("orderEntryProducer")
	private RabbitProducer orderEntryProducer;

	/** The payment receipt producer. */
	@Autowired
	@Qualifier("paymentReceiptProducer")
	private RabbitProducer paymentReceiptProducer;

	/** The printable wowcher producer. */
	@Autowired
	@Qualifier("printableWowcherProducer")
	private RabbitProducer printableWowcherProducer;

	/** The gift printable wowcher producer. */
	@Autowired
	@Qualifier("giftPrintableWowcherProducer")
	private RabbitProducer giftPrintableWowcherProducer;

	/** The delivery address deal gift email producer. */
	@Autowired
	@Qualifier("deliveryAddressDealGiftEmailProducer")
	private RabbitProducer deliveryAddressDealGiftEmailProducer;

	/** The deal voucher refund producer. */
	@Autowired
	@Qualifier("dealVoucherRefundProducer")
	private RabbitProducer dealVoucherRefundProducer;

	/** The customer subscription primary producer. */
	@Autowired
	@Qualifier("customerSubscriptionPrimaryProducer")
	private RabbitProducer customerSubscriptionPrimaryProducer;

	/** The customer subscription secondary producer. */
	@Autowired
	@Qualifier("customerSubscriptionSecondaryProducer")
	private RabbitProducer customerSubscriptionSecondaryProducer;

	/** The rpx producer. */
	@Autowired
	@Qualifier("rpxProducer")
	private RabbitProducer rpxProducer;

	/** The net storage ftp producer. */
	@Autowired
	@Qualifier("netStorageFtpProducer")
	private RabbitProducer netStorageFtpProducer;

	/** The front end password reset email producer. */
	@Autowired
	@Qualifier("frontEndPasswordResetEmailProducer")
	private RabbitProducer frontEndPasswordResetEmailProducer;

	/** The refund requested email for customer producer. */
	@Autowired
	@Qualifier("refundRequestedEmailForCustomerProducer")
	private RabbitProducer refundRequestedEmailForCustomerProducer;

	/** The refund requested email for merchant producer. */
	@Autowired
	@Qualifier("refundRequestedEmailForMerchantProducer")
	private RabbitProducer refundRequestedEmailForMerchantProducer;

	/** The refund declined email for customer producer. */
	@Autowired
	@Qualifier("refundDeclinedEmailForCustomerProducer")
	private RabbitProducer refundDeclinedEmailForCustomerProducer;

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		BasicConfigurator.configure();
	}
	
	/**
	 * Refund declined email for customer integer test.
	 */
	@Test
	public void refundDeclinedEmailForCustomerMapTest()
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("refundDeclinedEmailForCustomerTest()");
		}

		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("refundDeclinedEmailForCustomer", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			refundDeclinedEmailForCustomerProducer.sendMessage(strIntMap, "123" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}
	/**
	 * Refund requested email for merchant test.
	 */
	@Test
	public void refundRequestedEmailForMerchantTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("refundRequestedEmailForMerchant", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));
			
			refundRequestedEmailForMerchantProducer.sendMessage( strIntMap , "123");

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Refund requested email for customer test.
	 */
	@Test
	public void refundRequestedEmailForCustomerTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("refundRequestedEmailForCustomer", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));
			
			refundRequestedEmailForCustomerProducer.sendMessage(strIntMap, "33" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Front end password reset email test.
	 */
	@Test
	public void frontEndPasswordResetEmailTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("frontEndPasswordResetEmail", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			frontEndPasswordResetEmailProducer.sendMessage(strIntMap, "44" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Net storage ftp test.
	 */
	@Test
	public void netStorageFtpTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("netStorageFtp", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			netStorageFtpProducer.sendMessage(strIntMap, "55" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Rpx test.
	 */
	@Test
	public void rpxTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("rpx", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			rpxProducer.sendMessage(strIntMap, "66" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Customer subscription secondary test.
	 */
	@Test
	public void customerSubscriptionSecondaryTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("customerSubscriptionSecondary", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			customerSubscriptionSecondaryProducer.sendMessage(strIntMap, "77" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Customer subscription primary test.
	 */
	@Test
	public void customerSubscriptionPrimaryTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("customerSubscriptionPrimary", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			customerSubscriptionPrimaryProducer.sendMessage(strIntMap, "88" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Deal voucher refund test.
	 */
	@Test
	public void dealVoucherRefundTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("dealVoucherRefund", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			dealVoucherRefundProducer.sendMessage(strIntMap, "99" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Delivery address test.
	 */
	@Test
	public void deliveryAddressTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("deliveryAddress", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			deliveryAddressDealGiftEmailProducer.sendMessage(strIntMap, "1010" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Gift printable wowcher test.
	 */
	@Test
	public void giftPrintableWowcherTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("giftPrintableWowcher", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			giftPrintableWowcherProducer.sendMessage(strIntMap, "1111" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Printable wowcher test.
	 */
	@Test
	public void printableWowcherTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("printableWowcher", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			printableWowcherProducer.sendMessage(strIntMap, "1212" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Email test.
	 */
	@Test
	public void emailTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("email", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			emailProducer.sendMessage(strIntMap, "1313" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Order entry test.
	 */
	@Test
	public void orderEntryTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("orderEntry", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			orderEntryProducer.sendMessage(strIntMap, "1414" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Order entry test.
	 */
	@Test
	public void paymentReceiptTest()
	{
		try
		{
			Map<String, Integer> strIntMap = new HashMap<String, Integer>();
			strIntMap.put("paymentReceipt", Integer.valueOf(11));
			strIntMap.put("b", Integer.valueOf(22));
			strIntMap.put("c", Integer.valueOf(33));
			strIntMap.put("d", Integer.valueOf(44));		
		
			paymentReceiptProducer.sendMessage(strIntMap, "1515" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

}
