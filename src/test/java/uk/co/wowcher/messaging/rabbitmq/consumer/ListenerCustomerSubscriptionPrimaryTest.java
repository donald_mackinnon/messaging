package uk.co.wowcher.messaging.rabbitmq.consumer;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;
import uk.co.wowcher.messaging.rabbitmq.listeners.CustomerSubscriptionPrimaryListener;

import static org.junit.Assert.fail;

// TODO: Auto-generated Javadoc

/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.xml" })
// @ContextConfiguration(locations = { "classpath:/rabbitmq-producer-context.xml",
// "classpath:/rabbitmq-listener-context.xml" })
@ContextConfiguration(locations = {"classpath:/spring/test_rabbitmq-producer-context.xml", "classpath:/spring/test_customerSubscriptionPrimaryListener.xml"})
public class ListenerCustomerSubscriptionPrimaryTest
{

    private static final Logger logger = Logger.getLogger(ListenerCustomerSubscriptionPrimaryTest.class.getName());

    //private static RabbitProducer customerSubscriptionPrimaryProducer;
    /**
     * The customer subscription primary producer.
     */
    @Autowired
    @Qualifier("customerSubscriptionPrimaryListener")
    private CustomerSubscriptionPrimaryListener customerSubscriptionPrimaryListener;

    /**
     * Sets the up before cla   ss.
     *
     * @throws Exception the exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        BasicConfigurator.configure();
         logger.debug("ListenerCustomerSubscriptionPrimaryTest()");
    }

    /**
     * Customer subscription primary test.
     */
    @Test
    public void customerSubscriptionPrimaryTest() {

        logger.debug("customerSubscriptionPrimaryTest()");


    }

}
