package uk.co.wowcher.messaging.rabbitmq.producer;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.xml" })
// @ContextConfiguration(locations = { "classpath:/rabbitmq-producer-context.xml",
// "classpath:/rabbitmq-listener-context.xml" })
@ContextConfiguration(locations = {"classpath:/spring/rabbitmq-producer-context.xml",
    "classpath:/spring/deliveryAddressDealGiftEmailListener.xml"})
public class DeliveryAddressTest
{
    private static final Logger logger = Logger.getLogger(DeliveryAddressTest.class.getName());

	/** The delivery address deal gift email producer. */
	@Autowired
	@Qualifier("deliveryAddressDealGiftEmailProducer")
	private RabbitProducer deliveryAddressDealGiftEmailProducer;

	/**
	 * Sets the up before class.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
        BasicConfigurator.configure();
	}

    /**
     * Delivery address test.
     */
    @Test
    public void deliveryAddressTest()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("deliveryAddressTest()");
        }

        try
        {
            deliveryAddressDealGiftEmailProducer.sendMessage("deliveryAddressTest!", "1010");

            Thread.sleep(1000);

        }
        catch (Exception e)
        {
            fail("Test failed: " + e.getLocalizedMessage());
        }
    }


}
