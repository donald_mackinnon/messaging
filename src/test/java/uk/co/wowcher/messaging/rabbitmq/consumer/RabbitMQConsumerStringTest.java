package uk.co.wowcher.messaging.rabbitmq.consumer;

import static org.junit.Assert.fail;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;


// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.§§xml" })
@ContextConfiguration(locations = { "classpath:/rabbitmq-consumer-context.xml" })
public class RabbitMQConsumerStringTest
{
    private static final  Logger logger = Logger.getLogger(RabbitMQConsumerStringTest.class.getName());

	/** The rabbit producer. */
	@Autowired
	@Qualifier("emailProducer")
	private RabbitProducer emailProducer;

	/** The rabbit producer. */
	@Autowired
	@Qualifier("orderEntryProducer")
	private RabbitProducer orderEntryProducer;

	/** The payment receipt producer. */
	@Autowired
	@Qualifier("paymentReceiptProducer")
	private RabbitProducer paymentReceiptProducer;

	/** The printable wowcher producer. */
	@Autowired
	@Qualifier("printableWowcherProducer")
	private RabbitProducer printableWowcherProducer;

	/** The gift printable wowcher producer. */
	@Autowired
	@Qualifier("giftPrintableWowcherProducer")
	private RabbitProducer giftPrintableWowcherProducer;

	/** The delivery address deal gift email producer. */
	@Autowired
	@Qualifier("deliveryAddressDealGiftEmailProducer")
	private RabbitProducer deliveryAddressDealGiftEmailProducer;

	/** The deal voucher refund producer. */
	@Autowired
	@Qualifier("dealVoucherRefundProducer")
	private RabbitProducer dealVoucherRefundProducer;

	/** The customer subscription primary producer. */
	@Autowired
	@Qualifier("customerSubscriptionPrimaryProducer")
	private RabbitProducer customerSubscriptionPrimaryProducer;

	/** The customer subscription secondary producer. */
	@Autowired
	@Qualifier("customerSubscriptionSecondaryProducer")
	private RabbitProducer customerSubscriptionSecondaryProducer;

	/** The rpx producer. */
	@Autowired
	@Qualifier("rpxProducer")
	private RabbitProducer rpxProducer;

	/** The net storage ftp producer. */
	@Autowired
	@Qualifier("netStorageFtpProducer")
	private RabbitProducer netStorageFtpProducer;

	/** The front end password reset email producer. */
	@Autowired
	@Qualifier("frontEndPasswordResetEmailProducer")
	private RabbitProducer frontEndPasswordResetEmailProducer;

	/** The refund requested email for customer producer. */
	@Autowired
	@Qualifier("refundRequestedEmailForCustomerProducer")
	private RabbitProducer refundRequestedEmailForCustomerProducer;

	/** The refund requested email for merchant producer. */
	@Autowired
	@Qualifier("refundRequestedEmailForMerchantProducer")
	private RabbitProducer refundRequestedEmailForMerchantProducer;

	/** The refund declined email for customer producer. */
	@Autowired
	@Qualifier("refundDeclinedEmailForCustomerProducer")
	private RabbitProducer refundDeclinedEmailForCustomerProducer;

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		BasicConfigurator.configure();
	}
	
	/**
	 * Refund declined email for customer integer test.
	 */
	@Test
	public void refundDeclinedEmailForCustomerIntegerTest()
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("refundDeclinedEmailForCustomerTest()");
		}

		/*
		try
		{
			Integer val = new Integer(123456);
			refundDeclinedEmailForCustomerProducer.sendMessage(val, "123" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
		*/
	}

	/**
	 * Refund declined email for customer test.
	 */
	@Test
	public void refundDeclinedEmailForCustomerTest()
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("refundDeclinedEmailForCustomerTest()");
		}

		try
		{
			refundDeclinedEmailForCustomerProducer.sendMessage("refundDeclinedEmailForCustomerTest!", "123" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}



	/**
	 * Refund requested email for merchant test.
	 */
	@Test
	public void refundRequestedEmailForMerchantTest()
	{
		try
		{
			refundRequestedEmailForMerchantProducer.sendMessage("refundRequestedEmailForMerchantTest!", "22" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Refund requested email for customer test.
	 */
	@Test
	public void refundRequestedEmailForCustomerTest()
	{
		try
		{
			refundRequestedEmailForCustomerProducer.sendMessage("refundRequestedEmailForCustomerTest!", "33" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Front end password reset email test.
	 */
	@Test
	public void frontEndPasswordResetEmailTest()
	{
		try
		{
			frontEndPasswordResetEmailProducer.sendMessage("frontEndPasswordResetEmailTest!", "44" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Net storage ftp test.
	 */
	@Test
	public void netStorageFtpTest()
	{
		try
		{
			netStorageFtpProducer.sendMessage("netStorageFtpTest!", "55" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Rpx test.
	 */
	@Test
	public void rpxTest()
	{
		try
		{
			rpxProducer.sendMessage("rpxTest!", "66" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Customer subscription secondary test.
	 */
	@Test
	public void customerSubscriptionSecondaryTest()
	{
		try
		{
			customerSubscriptionSecondaryProducer.sendMessage("customerSubscriptionSecondaryTest!", "77" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Customer subscription primary test.
	 */
	@Test
	public void customerSubscriptionPrimaryTest()
	{
		try
		{
			customerSubscriptionPrimaryProducer.sendMessage("customerSubscriptionPrimaryTest!", "88" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Deal voucher refund test.
	 */
	@Test
	public void dealVoucherRefundTest()
	{
		try
		{
			dealVoucherRefundProducer.sendMessage("dealVoucherRefundTest!", "99" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Delivery address test.
	 */
	@Test
	public void deliveryAddressTest()
	{
		try
		{
			deliveryAddressDealGiftEmailProducer.sendMessage("deliveryAddressTest!", "1010" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Gift printable wowcher test.
	 */
	@Test
	public void giftPrintableWowcherTest()
	{
		/*
		try
		{
			giftPrintableWowcherProducer.sendMessage("giftPrintableWowcherTest!", "1111" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
		*/
	}

	/**
	 * Printable wowcher test.
	 */
	@Test
	public void printableWowcherTest()
	{
		try
		{
			printableWowcherProducer.sendMessage("printableWowcherTest!", "1212" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Email test.
	 */
	@Test
	public void emailTest()
	{
		try
		{
			emailProducer.sendMessage("emailTest!", "1313" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Order entry test.
	 */
	@Test
	public void orderEntryTest()
	{
		try
		{
			orderEntryProducer.sendMessage("orderEntryTest", "1414" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

	/**
	 * Order entry test.
	 */
	@Test
	public void paymentReceiptTest()
	{
		try
		{
			paymentReceiptProducer.sendMessage("paymentReceiptTest", "1515" );

			Thread.sleep(1000);

		}
		catch (Exception e)
		{
			fail("Test failed: " + e.getLocalizedMessage());
		}
	}

}
