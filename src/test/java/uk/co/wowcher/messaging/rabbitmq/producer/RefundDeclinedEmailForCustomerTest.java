package uk.co.wowcher.messaging.rabbitmq.producer;

import static org.junit.Assert.fail;

import org.apache.log4j.Logger;
import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import uk.co.wowcher.messaging.rabbitmq.RabbitProducer;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQProducerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "/uk/test-jms-rabbitmq-context.xml" })
// @ContextConfiguration(locations = { "classpath:/rabbitmq-producer-context.xml",
// "classpath:/rabbitmq-listener-context.xml" })
@ContextConfiguration(locations = {"classpath:/spring/rabbitmq-producer-context.xml", "classpath:/spring/refundDeclinedEmailForCustomerListener.xml"})
public class RefundDeclinedEmailForCustomerTest
{
    private static final Logger logger = Logger.getLogger(RefundDeclinedEmailForCustomerTest.class.getName());

	/** The refund declined email for customer producer. */
	@Autowired
	@Qualifier("refundDeclinedEmailForCustomerProducer")
	private RabbitProducer refundDeclinedEmailForCustomerProducer;

	/**
	 * Sets the up before class.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
        BasicConfigurator.configure();
	}

    /**
     * Refund declined email for customer test.
     */
    @Test
    public void refundDeclinedEmailForCustomerTest()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("refundDeclinedEmailForCustomerTest()");
        }

        try
        {
            refundDeclinedEmailForCustomerProducer.sendMessage("refundDeclinedEmailForCustomerTest!", "123");

            Thread.sleep(1000);

        }
        catch (Exception e)
        {
            fail("Test failed: " + e.getLocalizedMessage());
        }
    }

    /**
     * Refund declined email for customer integer test.
     */
    @Test
    public void refundDeclinedEmailForCustomerIntegerTest()
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("refundDeclinedEmailForCustomerTest()");
        }

        try
        {
            Integer val = new Integer(123456);
            refundDeclinedEmailForCustomerProducer.sendMessage(val, "123");

            Thread.sleep(1000);

        }
        catch (Exception e)
        {
            fail("Test failed: " + e.getLocalizedMessage());
        }
    }


}
